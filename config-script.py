import sys


def change(aws_access_key_id, aws_secret_access_key, vol_id, stage):
    data = ''
    with open('config.yaml', 'r+') as file:
        data = file.read().format(aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key,
                                  vol_id=vol_id, stage=stage)
        file.seek(0)
        file.truncate()
        file.write(data)
    file.close()


change(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
