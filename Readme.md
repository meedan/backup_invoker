**Bakup Invoker** 

This project generally is designed as a `lambda` function to be deployed to `AWS` to backup any needed volumes

It is specifically designed to take a snapshot for `ES` storage for Alegre

This `lambda` function is triggered by AWS `cloudwatch event rule` 

    - Go to AWS console
    - Go to Cloudwatch
    - Choose Events => Rules
    - Create the rule with the cron expression needed
    - Assign name, description, and lambda function for this rule

How to use?

    - Config-script.py takes 4 main arguments:
        1- aws_access_key_id (user must have s3 and lambda full access, EC2 read, write, trigger access)
        2- aws_secret_access_key
        3- vol_id (the vol_id to be backedup)
        4- stage (whether qa or live)
    - Create a trigger for the lambda function, we have several options:
        1- scheduled cron task
        2- API gateway
        3- Run it manually from AWS console
    - Snapshots will be recreated once the function is triggered.
    
Pending tasks:

    - Delete old snapshots in order not to accumulate snapshots in AWS
        
        
                                              