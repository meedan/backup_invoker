import boto3
import datetime
import os

today = datetime.date.today()
today_string = today.strftime('%Y/%m/%d')
vol_ids = [os.getenv("vol_id")]

client = boto3.client('ec2')


def lambda_handler(event, context):
    snapshot_counter = 0
    snap_size_counter = 0
    name = "{}-ecs-es".format(os.getenv("stage"))
    for v in vol_ids:
        snapshot = client.create_snapshot(
            VolumeId = v,
            Description = 'AutoSnapshot of {0}, on volume {1} - Created {2}'.format(name, v, today_string),
            TagSpecifications=[{
                'ResourceType': 'snapshot',
                "Tags": [
                    {
                        'Key': 'auto_snap',
                        'Value': 'true'
                    },
                    {
                        'Key': 'volume',
                        'Value': v
                    },
                    {
                        'Key': 'CreatedOn',
                        'Value': today_string
                    },
                    {
                        'Key': 'Name',
                        'Value': '{0} {1} autosnap'.format(name, today_string)
                    }
                ]
            }]
        )
        print('Snapshot completed')
        snapshot_counter += 1
    print('Made {0} snapshots totalling {1} GB'.format(snapshot_counter, snap_size_counter))
    return
